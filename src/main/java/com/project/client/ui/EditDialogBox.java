package com.project.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.project.client.BookService;
import com.project.client.BookServiceAsync;
import com.project.shared.BookResp;

import java.sql.Date;

public class EditDialogBox extends DialogBox {
    interface EditDialogBoxCUiBinder extends UiBinder<HTMLPanel, EditDialogBox> {
    }

    private static EditDialogBoxCUiBinder ourUiBinder = GWT.create(EditDialogBoxCUiBinder.class);

    @UiField
    TextBox authorTextBox;
    @UiField
    TextBox nameTextBox;
    @UiField
    TextBox descriptionTextBox;
    @UiField
    TextBox dateTextBox;
    @UiField
    Button okButton;
    @UiField
    Button cancelButton;

    public EditDialogBox(BookResp bookResp) {
        setWidget(ourUiBinder.createAndBindUi(this));

        setStyleName("container-fluid");
        setAnimationEnabled(true);
        setGlassEnabled(true);

        okButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                hide();
                final BookServiceAsync service = GWT.create(BookService.class);
                service.save(bookResp, new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(Void result) {
                        Window.Location.reload();
                    }
                });
            }
        });

        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });

        authorTextBox.setText(bookResp.getAuthor());
        nameTextBox.setText(bookResp.getName());
        descriptionTextBox.setText(bookResp.getDescription());
        if (bookResp.getCreated() == null) {
            dateTextBox.setText("");
        } else {
            dateTextBox.setText(bookResp.getCreated());
        }

        authorTextBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                bookResp.setAuthor(authorTextBox.getText());
            }
        });

        nameTextBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                bookResp.setName(nameTextBox.getText());
            }
        });

        descriptionTextBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                bookResp.setDescription(descriptionTextBox.getText());
            }
        });

        dateTextBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                if (validateDate(dateTextBox.getText())) {
                    bookResp.setCreated(dateTextBox.getText());
                }
            }
        });



    }

    private boolean validateDate(String date) {
        try {
            Date date1 = Date.valueOf(date);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}