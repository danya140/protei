package com.project.client.ui;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.ListDataProvider;
import com.project.client.BookService;
import com.project.client.BookServiceAsync;
import com.project.shared.BookResp;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Главная страница
 */
public class BookList extends Composite {
    interface BookListUiBinder extends UiBinder<HTMLPanel, BookList> {
    }

    private static BookListUiBinder ourUiBinder = GWT.create(BookListUiBinder.class);

    @UiField
    CellTable<BookResp> cellTable = new CellTable<>();

    @UiField
    Button addButton = new Button();

    @UiField
    Button deleteButton = new Button();

/*    @UiField
    Button sortByAuthorButton = new Button();

    @UiField
    Button sortByNameButton = new Button();*/

    @UiField
    TextBox authorSearchTextBox;

    @UiField
    TextBox nameSearchTextBox;

    private List<BookResp> deleteList = new ArrayList<>();
    private ListDataProvider<BookResp> dataProvider = new ListDataProvider<>();


    public BookList() {
        initWidget(ourUiBinder.createAndBindUi(this));
        initCellTable();
        initSortButtons();
        initButtonMenu();
    }

    /**
     * Инициализация кнопок для управления таблицей
     */
    private void initButtonMenu() {
        addButton.setText("Добавить");
        addButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CreateDialogBox createDialogBox = new CreateDialogBox();
                createDialogBox.show();
            }
        });
        deleteButton.setText("Удалить");
        deleteButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final BookServiceAsync service = GWT.create(BookService.class);
                service.deleteMultiple(deleteList, new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        System.out.println();
                    }

                    @Override
                    public void onSuccess(Void result) {
                        Window.Location.reload();
                    }
                });
            }
        });
    }

    /**
     * Инициализация поиска
     */
    private void initSortButtons() {
        authorSearchTextBox.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                String id = authorSearchTextBox.getText();
                List<BookResp> list = dataProvider.getList();
                List<BookResp> newList = new ArrayList<>();

                if (list.isEmpty() || id.isEmpty()) {
                    initDataProviderList();
                }

                for (BookResp bookResp : list) {
                    if ((bookResp.getAuthor()).contains(id)) {
                        newList.add(bookResp);
                    }
                }
                dataProvider.setList(newList);
            }
        });

        nameSearchTextBox.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                String name = nameSearchTextBox.getText();
                List<BookResp> list = dataProvider.getList();
                List<BookResp> newList = new ArrayList<>();

                if (list.isEmpty() || name.isEmpty()) {
                    initDataProviderList();
                }

                for (BookResp bookResp : list) {
                    if ((bookResp.getName()).contains(name)) {
                        newList.add(bookResp);
                    }
                }
                dataProvider.setList(newList);
            }
        });
    }

    /**
     * Инициализирует список датапровайдера
     */
    private void initDataProviderList() {
        final BookServiceAsync service = GWT.create(BookService.class);
        service.list(new AsyncCallback<List<BookResp>>() {
            @Override
            public void onFailure(Throwable caught) {
                GWT.log("Cant get list");
            }

            @Override
            public void onSuccess(List<BookResp> result) {
                dataProvider.getList().removeAll(dataProvider.getList());
                dataProvider.getList().addAll(result);
            }
        });
    }

    /**
     * Инициализация таблицы
     */
    private void initCellTable() {
        TextColumn<BookResp> idColumn = new TextColumn<BookResp>() {
            @Override
            public String getValue(BookResp bookResp) {
                return bookResp.getId() + "";
            }
        };
        idColumn.setSortable(true);
        cellTable.addColumn(idColumn, "Id");

        TextColumn<BookResp> nameColumn = new TextColumn<BookResp>() {
            @Override
            public String getValue(BookResp bookResp) {
                return bookResp.getName();
            }
        };
        nameColumn.setSortable(true);
        cellTable.addColumn(nameColumn, "Название");

        TextColumn<BookResp> authorColumn = new TextColumn<BookResp>() {
            @Override
            public String getValue(BookResp bookResp) {
                return bookResp.getAuthor();
            }
        };
        authorColumn.setSortable(true);
        cellTable.addColumn(authorColumn, "Автор");

        TextColumn<BookResp> descriptionColumn = new TextColumn<BookResp>() {
            @Override
            public String getValue(BookResp bookResp) {
                return bookResp.getDescription();
            }
        };
        descriptionColumn.setSortable(true);
        cellTable.addColumn(descriptionColumn, "Описание");

        TextColumn<BookResp> dateColumn = new TextColumn<BookResp>() {
            @Override
            public String getValue(BookResp bookResp) {
                if (bookResp.getCreated() == null) {
                    return "";
                } else {
                    return bookResp.getCreated();
                }
            }
        };
        dateColumn.setSortable(true);
        cellTable.addColumn(dateColumn, "Дата написания");

        Column<BookResp, Boolean> delColum = new Column<BookResp, Boolean>(new CheckboxCell()) {
            @Override
            public Boolean getValue(BookResp object) {
                return false;
            }
        };
        delColum.setFieldUpdater(new FieldUpdater<BookResp, Boolean>() {
            @Override
            public void update(int index, BookResp object, Boolean value) {
                if (deleteList.contains(object)) {
                    deleteList.remove(object);
                } else {
                    deleteList.add(object);
                }
            }
        });
        cellTable.addColumn(delColum);

        ActionCell.Delegate delegate = new ActionCell.Delegate<BookResp>() {
            @Override
            public void execute(BookResp object) {
                EditDialogBox editDialogBox = new EditDialogBox(object);
                editDialogBox.show();
            }
        };
        Column<BookResp, BookResp> editColumn = new Column<BookResp, BookResp>(new ActionCell<BookResp>("Edit", delegate)) {
            @Override
            public BookResp getValue(BookResp object) {
                return object;
            }
        };
        cellTable.addColumn(editColumn);

        dataProvider.addDataDisplay(cellTable);
        initDataProviderList();

        ColumnSortEvent.ListHandler sortHandler = new ColumnSortEvent.ListHandler<BookResp>(dataProvider.getList());
        sortHandler.setComparator(idColumn, new Comparator<BookResp>() {
            @Override
            public int compare(BookResp o1, BookResp o2) {
                return o1.getId() - o2.getId();
            }
        });
        sortHandler.setComparator(nameColumn, new Comparator<BookResp>() {
            @Override
            public int compare(BookResp o1, BookResp o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        sortHandler.setComparator(authorColumn, new Comparator<BookResp>() {
            @Override
            public int compare(BookResp o1, BookResp o2) {
                return o1.getAuthor().compareTo(o2.getAuthor());
            }
        });
        sortHandler.setComparator(descriptionColumn, new Comparator<BookResp>() {
            @Override
            public int compare(BookResp o1, BookResp o2) {
                return o1.getDescription().compareTo(o2.getDescription());
            }
        });
        sortHandler.setComparator(dateColumn, new Comparator<BookResp>() {
            @Override
            public int compare(BookResp o1, BookResp o2) {
                Date date1 = Date.valueOf(o1.getCreated());
                Date date2 = Date.valueOf(o2.getCreated());
                return date1.compareTo(date2);
            }
        });

        cellTable.addColumnSortHandler(sortHandler);
    }
}