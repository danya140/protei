package com.project.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.project.shared.BookResp;

import java.util.List;

/**
 * RPC для работы с базой
 */
@RemoteServiceRelativePath("book")
public interface BookService extends RemoteService {

    /**
     * Получение списка всех книг
     *
     * @return список всех книг
     */
    List<BookResp> list();

    /**
     * Сохранение книги
     *
     * @param bookResp книга
     */
    void save(BookResp bookResp);


    /**
     * Удаление книг
     *
     * @param deleteList списко книг для удаления
     */
    void deleteMultiple(List<BookResp> deleteList);
}
