package com.project.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.project.client.ui.BookList;

/**
 * Entry point class
 */
public class Project implements EntryPoint {

    public void onModuleLoad() {
        BookList bookList = new BookList();
        RootPanel.get().add(bookList);
    }
}
