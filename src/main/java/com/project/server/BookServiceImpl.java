package com.project.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.project.client.BookService;
import com.project.server.repository.BookRepository;
import com.project.shared.BookResp;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Сервис по работе с базой
 */
public class BookServiceImpl extends RemoteServiceServlet implements BookService {

    /**
     * репозиторий
     */
    private final BookRepository repo = AppContext.getBean(BookRepository.class);

    public List<BookResp> list() {
        List<BookResp> list = new ArrayList<>();
        for (BookEntity entity : repo.findAll()) {
            list.add(new BookResp(entity.getId(),
                    entity.getName(),
                    entity.getAuthor(),
                    entity.getDescription(),
                    StringUtils.defaultString(entity.getCreated(), "")));
        }
        return list;
    }

    public void save(BookResp bookResp) {
        BookEntity entity = new BookEntity();
        entity.setId(bookResp.getId());
        entity.setName(bookResp.getName());
        entity.setAuthor(bookResp.getAuthor());
        entity.setDescription(bookResp.getDescription());
        if (StringUtils.isEmpty(bookResp.getCreated())) {
            entity.setCreated(null);
        } else {
            entity.setCreated(bookResp.getCreated());
        }
        repo.save(entity);
    }

    public void deleteMultiple(List<BookResp> deleteList) {
        for (BookResp bookResp : deleteList) {
            repo.delete(bookResp.getId());
        }
    }
}
