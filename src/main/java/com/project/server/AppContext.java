package com.project.server;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Контекст приложения
 */
public class AppContext implements ApplicationContextAware {

    private static ApplicationContext appContext;

    private AppContext() {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        appContext = applicationContext;
    }

    /**
     * Получения бина
     *
     * @param clazz класс бина
     * @param <T>   параметры
     * @return бин
     */
    public static <T> T getBean(Class<T> clazz) {
        return appContext.getBean(clazz);
    }
}